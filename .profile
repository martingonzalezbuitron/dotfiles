# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022


# 2019-08-29 MGB
# set XDG Base Directory for user enviroment variables
# See these websites:
# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
# https://wiki.archlinux.org/index.php/XDG_Base_Directory
# https://help.ubuntu.com/community/EnvironmentVariables#List_of_common_environment_variables
if [ -z "$XDG_CONFIG_HOME" ]; then
	#include env
	export XDG_CONFIG_HOME="$HOME"/.config
fi

if [ -z "$XDG_CACHE_HOME" ]; then
	#include env
	export XDG_CACHE_HOME="$HOME"/.cache
fi

if [ -z "$XDG_DATA_HOME" ]; then
	#include env
	export XDG_DATA_HOME="$HOME"/.local/share
fi


# 2019-08-29 MGB
# Add custom bin directories to PATH.
# Following: https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s13.html
if [ -d "/opt/bin" ] ; then
    PATH=/opt/bin:"$PATH"
fi

if [ -d "/snap/bin" ] ; then
    PATH=/snap/bin:"$PATH"
fi

if [ -d "$HOME/.bin" ] ; then
    PATH="$HOME/.bin:$PATH"
fi

if [ -d "$XDG_DATA_HOME/bin" ] ; then
    PATH="$XDG_DATA_HOME/bin":"$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi


# 2019-08-29 MGB
# Set up others environments variables
source "$XDG_CONFIG_HOME"/bash/env.bash

# 2020-03-18, OLD -> 2019-08-30 MGB
# Adding manually this env taken from https://wiki.archlinux.org/index.php/XDG_Base_Directory
# Environment variable for ~/.gnupg
export GNUPGNOME="$XDG_DATA_HOME"/gnupg
# Environment variable for ~/.ICEauthority
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
# Environment variable for ~/.npm
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
# Environment variable for ~/.xinitrc
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
# Environment variable for ~/.xserverrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
# Environment variable for ~/.wget-hsts
export WGETRC="$XDG_CONFIG_HOME"/wgetrc
# Environment variable for ~/.ipython
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
# Environment variable for ~/.jupyter
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
# Environment variable for ~/go
export GOPATH="$XDG_CONFIG_HOME"/go-"${GO_VERSION}"/go
# Environment variable for ~/.pyenv
export PYENV_ROOT="$XDG_CONFIG_HOME"/pyenv
export PATH="${PYENV_ROOT}/bin:$PATH"
# Environment variable for ~/.inputrc
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
# Environment variable for ~/.local/pipx
export PIPX_HOME="$XDG_DATA_HOME"/pipx

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi
