# Enable colors.

# Note that BSD ls (Mac) and GNU ls (Linux) have different flags for
# toggling color on command output. Therefore we have to check which
# OS are we supporting.

case `uname` in
    Darwin*)
        alias ls='ls -G'
        alias grep='grep --color=always'
        alias fgrep='fgrep --color=always'
        alias egrep='egrep --color=always'
        ;;
    Linux*)
        if [ -x /usr/bin/dircolors ]; then
            alias ls='ls --color=always'
            alias grep='grep --color=always'
            alias fgrep='fgrep --color=always'
            alias egrep='egrep --color=always'
        fi
        ;;
esac
