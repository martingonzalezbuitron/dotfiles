# This really works?
function __prompt__exitcode() {
    # Tints the prompt red if the last executed command returns non-zero
    local EXIT_CODE="$?"
    [ $EXIT_CODE != 0 ] && echo -e "\001\033[1;31m\002"
}

