# Bash-completions

# 2021-03-09 MGB
# Apptainer
[[ -s "/opt/apptainer-1.2.3/share/bash-completion/completions/apptainer" ]] && source /opt/apptainer-1.2.3/share/bash-completion/completions/apptainer
#singularity
[[ -s "/opt/singularity-v3.7.1/etc/bash_completion.d/singularity" ]] && source /opt/singularity-v3.7.1/etc/bash_completion.d/singularity
#git
[[ -s "/usr/share/git-core/git-prompt.sh" ]] && source /usr/share/git-core/git-prompt.sh
#mmseqs
[[ -s "/opt/mmseqs2-13-45111/util/bash-completion.sh" ]] && source /opt/mmseqs2-13-45111/util/bash-completion.sh
#tmux
[[ -s "/opt/tmux-3.3a/tmux-bash-completion/completions/tmux" ]] && source /opt/tmux-3.3a/tmux-bash-completion/completions/tmux
# Source goto
[[ -s "/usr/local/share/goto.sh" ]] && source /usr/local/share/goto.sh
# Stack
[[ -s "~/.stack/bash-completion-stack.sh" ]] && source ~/.stack/bash-completion-stack.sh
# frictionless
[[ -s "~/.bash_completions/frictionless.sh" ]] && source ~/.bash_completions/frictionless.sh
# Pipx
eval "$(register-python-argcomplete pipx)"
# uv
eval "$(uv generate-shell-completion bash)"
# uvx
eval "$(uvx --generate-shell-completion bash)"
