# System
alias ll='ls -ltrFh'
alias la='ls -A'
alias cp='cp -ia'                 # confirm before overwriting something.
alias df='df -h -t ext4'          # human-readable sizes and only ext4.
alias free='free -h'              # show sizes in human-readable sizes.
alias tree='tree -phtDC'          # tree color,human-read,time-mod,permissions.
alias du='du -sch' 		          # show size and total of directory in h-read.
alias diff='colordiff'            # show diff with colors
alias cat='bat --paging=never'    # show colors using bat
alias bat='bat --paging=always'   # show colors always paging as less does
#alias less='less -R'              # show ANSI colors if exists
alias grep='grep --color=auto'
alias delete_0k_files_in_place='find . -maxdepth 1 -type f -size 0k -delete'
alias detect_0k_files_in_place='find . -maxdepth 1 -type f -size 0k'
alias today='date +%Y-%m-%d'
alias mydotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias lout='gnome-session-quit --no-prompt'

# Specific software
alias freecad='/opt/bin/FreeCAD_0.19-24291-Linux-Conda_glibc2.12-x86_64.AppImage &> /dev/null &'
alias alpine="alpine -p $XDG_CONFIG_HOME/alpine/pinerc -passfile $XDG_CONFIG_HOME/alpine/.pine-passfile -pwdcertdir $XDG_CONFIG_HOME/alpine/alpine-smime"
alias btop='btop-desktop'

## virtual-env aliasses
alias act-codnas-rna-2020_py36_virenv='source ~/Projects/2020/CoDNaS-RNA/.codnas-rna_virenv/bin/activate'
alias act-arn_py36_virenv='source ~/Projects/2019/ARNs_2019/arn_py36_env/bin/activate'
alias act-revenant_3d_modelling-2020_py36_virenv='source ~/Projects/2020/Revenant_3D_modelling/.revenant_3d_modelling_virenv/bin/activate'
alias act-mi_ipa-2020_py36_virenv='source ~/Tools/ElofssonLab/MI_IPA/.mi_ipa_virenv/bin/activate'
alias act-rcsb_serv_venv3102='source ~/Projects/2020/RCSB_services/.rcsb_serv_venv3102/bin/activate'
