# Check if command exists. Actually just runs `command -v` silently.
exists() { command -v "${1}" >/dev/null 2>&1 && return 0 || return 1; }

mkproject() {
: '
Purpose: Create a project folder with subfolders (scripts,data,doc,
         inputs,outputs). Use "-h" for help.
Preconditions: For project name do not use spaces.
               You need priviliges to create a folder in the directory
               where you want to create it.
Parameters: 1- project name                           #string
            2- directory where to create the project  #string
TODO: * Third parameter to add more folder/s.
      * Check if project name has "/" inside.
      * Add new functionality for projects using a new feature
        with a file like "config.ini" where all enviromental
	projects variables are activated when some command is
	executed inside that project. For example: when you what
	to refere to latest update path inside the tree project
	directory.
      * In addition of last functionality, this mkproject function
        should handle another and better flags arguments. For example:
	if you want to create another time branch with subfolders
	structure.
'
    #Cheking parameters
    if [[ "${#}" -ge 1 && "${#}" -le 2 && "${1}" != "--help" && "${1}" != "-help" && "${1}" != "-h" && "${1}" != "--h" ]]
    then
        #Creating project
        project_name="${1}"
        #Checking if exist the directory given
        if [[ -z "${2}" ]]
        then
            #Cheking if the name of the project exists.
            if [[ -n "${project_name}" ]]
            then
                #Cheking that the name is not a directory
                if [[ ! -d "${project_name}" ]]
                then
                    #Creting the folder structure
                    mkdir -p "${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                    touch "${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                    if [[ ! -s "${project_name}"/README.md ]]
                    then
                        printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_name}"/README.md
                    fi
                else
                    #Creating the structure of the project allthough the
                    #folder name already exists
                    echo "${project_name} is a directory and already exist."
                    echo "Building the structure of folders if need it ..."
                    mkdir -p "${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                    touch "${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                    if [[ ! -s "${project_name}"/README.md ]]
                    then
                        printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_name}"/README.md
                    fi
                    echo "Done !"
                    echo "Check yourself if everythings is Ok."
                fi
            else
                echo "Error: you must proportionate a project's name."
            fi
        else
            #Checking the last character in the project's name.
            if [[ "${2: -1}" == "/" ]]
            then
                project_dir="${2%/}"
            else
                project_dir="${2}"
            fi
            #Checking if project_dir is a directory.
            if [[ -d "${project_dir}" ]]
            then
                #Checking if project_name is not empty
                if [[ -n "${project_name}" ]]
                then
                    #Checking if "${project_dir}"/"${project_name}" is exist.
                    if [[ ! -d "${project_dir}/${project_name}" ]]
                    then
                        #Creting the folder structure
                        mkdir -p "${project_dir}/${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                        touch "${project_dir}/${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                        if [[ ! -s "${project_dir}/${project_name}"/README.md ]]
                        then
                            printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_dir}/${project_name}"/README.md
                        fi
                    else
                        #Creating the structure of the project allthough the
                        #folder name already exists.
                        echo "${project_name} is a directory and already exist."
                        echo "${project_dir}/${project_name}"
                        echo "Building the structure of folders if need it ..."
                        mkdir -p "${project_dir}/${project_name}"/{src/scripts,data,processed_data,doc,results,outputs,tests}
                        touch "${project_dir}/${project_name}"/{README.md,src/{LICENSE,requirements.txt}}
                        if [[ ! -s "${project_dir}/${project_name}"/README.md ]]
                        then
                            printf "#<TITLE>\n\n##<SUBTITLE>\n\n###<3er-TITLE-LEVEL>\n\nWritten by Martín González Buitrón\nEmail: martingonzalezbuitron@gmail.com\n" > "${project_dir}/${project_name}"/README.md
                        fi
                        echo "Done !"
                        echo "Check yourself if everythings is Ok."
                    fi
                else
                    echo "Error: you must proportionate a project's name."
                fi
            else
                echo "Error: your ${project_dir} is not a directory."
            fi
        fi
    #Checking the number or parameters
    elif [[ "${#}" -gt 2 ]]
    then
        echo "Error: Too many arguments."
        echo "Try with --help option"
    elif [[ "${#}" -eq 0 ]]
    then
        echo "Error: You need to put at least the project's name (without spaces)."
        echo "Try with --help option"
    #Printing help section
    elif [[ "${1}" == "--help" || "${1}" == "-help" || "${1}" == "-h" || "${1}" == "--h" ]]
    then
        echo "--------------------------------------------------------"
        echo "Usage:"
        echo "------"
        echo "       mkproject <PROJECT-NAME> <PROJECT-DIRECTORY>"
        echo
        echo
        echo "Purpose: Create a project folder with subfolders"
        echo "-------  (data,doc,results,outputs,processed_data,src,tests)."
        echo
        echo
        echo "Positional arguments:"
        echo "---------------------"
        echo
        echo "First argument  (\$1): PROJECT NAME           // string"
        echo "Second argument (\$2): PROJECT DIRECTORY      // string"
        echo
        echo
        echo "Optional:"
        echo "---------"
        echo
        echo "    --help"
        echo
        echo "--------------------------------------------------------"
    fi
}

squash_and_mount() {
if [[ "${#}" -eq 2 && "${1}" != "--help" && "${1}" != "-help" && "${1}" != "-h" ]]
then
    #Check if the firt argument is a directory.
    if [[ -d "${1}" ]]
    then
        #Check if squashfs is v4.4 or higher
#        sqfs_version=$(bc -l <<< "$(mksquashfs -version | grep -m 1 "version" | cut -d ' ' -f 3)")
        sqfs_version=$(mksquashfs -version | grep -m 1 "version" | cut -d ' ' -f 3)
        sqfs_version_int=$(bc -l <<< "$(echo ${sqfs_version} | cut -d '.' -f 1)")
        sqfs_version_dec=$(bc -l <<< "$(echo ${sqfs_version} | cut -d '.' -f 2)")
        #Zstandard is supported. Use zstd to compress.
        if [[ "${sqfs_version_int}" -ge 4 && "${sqfs_version_dec}" -ge 4 ]]
        then
            #Create mount directory
            if [[ -n "${2}" ]]
            then
                parent_path=$(dirname "${2}")
                #Creating folder to mount and the squashfs to save the $1.sfs
                if [[ ! -e "${2}" && -d "${parent_path}" && "${2}" != "${parent_path}/squashfs" || "${1}" == "${2}" ]]
                then
                    #Getting the input and output folder name
                    output_folder_name=$(basename "${2}")
                    input_folder_name=$(basename "${1}")
                    if [[ ! -e "${parent_path}/squashfs/${input_folder_name}.sfs" ]]
                    then
                        mkdir -p "${parent_path}/squashfs"
                        mksquashfs "${1}" "${parent_path}/squashfs/${input_folder_name}.sfs" -comp zstd
                        mkdir -p "${2}"
                        if [[ -e "${parent_path}/squashfs/${input_folder_name}.sfs" ]]
                        then
                            mv "${1}" "${1}.old"
                            mount "${parent_path}/squashfs/${input_folder_name}.sfs" "${parent_path}/${output_folder_name}" -t squashfs -o loop
                            echo "Squashing and mounting has finished."
                            echo "Adding mounting folder to /etc/fstab ..."
                            sed -i "$ a ${parent_path}/squashfs/${input_folder_name}.sfs ${parent_path}/${output_folder_name}       squashfs        ro,defaults     0 0" /etc/fstab
                            echo "Mounting folder added to /etc/fstab."
                        else
                            echo "Warning! Something happend with the squashing step."
 #                           exit
                        fi
                    else
                        echo "Using ${parent_path}/squashfs/${input_folder_name}.sfs that already exist."
                        mkdir -p "${2}"
                        if [[ -e "${parent_path}/squashfs/${input_folder_name}.sfs" ]]
                        then
                            mv "${1}" "${1}.old"
                            mount "${parent_path}/squashfs/${input_folder_name}.sfs" "${parent_path}/${output_folder_name}" -t squashfs -o loop
                            echo "Squashing and mounting has finished."
                            echo "Adding mounting folder to /etc/fstab ..."
                            sed -i "$ a ${parent_path}/squashfs/${input_folder_name}.sfs ${parent_path}/${output_folder_name}       squashfs        ro,defaults     0 0" /etc/fstab
                            echo "Mounting folder added to /etc/fstab."
                        else
                            echo "Warning! Something happend with the squashing step."
#                            exit
                        fi
                    fi
                else
                    echo "Check if ${2} is written well. Should not exist."
                fi
            else
                echo "You need to give an output-path to mount the squashfs."
            fi
        else
            echo "Your squashfs version is older than 4.4 and do not support Zstandard compression."
        fi
    else
        echo "Check if ${1} exist and is a directory."
    fi
else
    echo "--------------------------------------------------------"
    echo "Usage:"
    echo "       squash_and_mount <INPUT-FOLDER> <OUTPUT-FOLDER>"
    echo
    echo "Mandatory arguments:"
    echo "==================="
    echo " 1- <INPUT-FOLDER>     // To create squashfs file // string"
    echo " 2- <OUTPUT-FOLDER>    // To be mounted           // string"
    echo
    echo "Dependencies:"
    echo "============"
    echo " Squashfs      --version >=4.4"
    echo " Zstandard     --version <all>"
    echo
    echo "Observation:"
    echo "==========="
    echo " SQUASHING - When squashing is finished, <INPUT-FOLDER>.sfs"
    echo " file is going to be saved in a new folder at the same"
    echo " directory level as <OUTPUT-FOLDER>."
    echo " MOUNTING - To mount <INPUT-FOLDER>.sfs as <OUTPUT-FOLDER>"
    echo " permantly, you need to run this as script with sudo."
    echo " A permanent absolute <OUTPUT-FOLDER> path is going to be"
    echo " pasted in /etc/fstab file."
    echo
    echo
    echo "Author: Martín González Buitrón"
    echo "email: <maritngonzalezbuitron@gmail.com>"
    echo "Institution: Universidad Nacional de Quilmes, Argentina."
    echo "--------------------------------------------------------"
fi
}

zstd_tunnel_compression(){
: '
Use like this:
zstd_tunnel_compression <FOLDER-NAME-IN-SCRATCH> <YOUR-OUTPUT-NAME> --turbo=<on or off>

Purpose:
    Use zstd with /scratch and tar for your
    scripts outputs.
Parameters
    {1} - directory for your outputs in /scratch/        // string
    {2} - your output name (file or directory)           // string
    {3} - turbo mode (on or off)                         // string ; default: --turbo=on
'
    #Cheking parameters
    if [[ "${#}" -le 3 && "${1}" != "--help" && "${1}" != "-help" && "${1}" != "-h" && "${1}" != "--h" ]]
    then
        echo "Initializing zstd_tunnel_compression() ..."
        #Checking if exist the directory given to use in /scratch
        echo "Checking if exist the directory given to use in /scratch is NOT a file.."
        if [[ -n "${1}" && ! -f "/scratch/${1}" ]]
        then
            #Creating output path in /scratch
            output_path=/scratch/"${1}"
            mkdir -p "${output_path}"
            #Cheking if the output name file exists and is not a directory
            echo "Checking if the output name file exists and is not a directory.."
            if [[ -n "${2}" && -f "${2}" ]]
            then
                echo "Output name file exists and is NOT a directory!!"
                #Creating the absolute output path name
                output_path_user=$(dirname "${2}")
                output_name=$(basename "${2}")
                #Creting the folder structure
                mkdir -p "${output_path_user}"
                #Run zstd
                echo "Trying to run zstd software.."
                zstd_bin=$(which zstd)
                #Testing tar with PIGZ
                #pigz_bin=$(which pigz)
                if [[ -x "${zstd_bin}" ]]
                then
                    echo "Running zstd.."
                    #Checking turbo mode ON or OFF
                    if [[ -n "${3}" && "${3}" == "--turbo=off" ]]
                    then
                        echo "Turbo-mode OFF !!"
                        # Use the following line if you want to compress and move an entire file NOT in tubo mode
                        time tar -cvf - "${output_path}"/"${output_name}" | "${zstd_bin}" -o "${output_path_user}"/"${output_name}".tar.zst
                        echo "zstd_tunnel_compression have finished successed"
                    else
                        echo "Turbo-mode ON (by default) !!"
                        # Use the following line if you want to compress and move an entire file in tubo mode
                        time tar -cvf - "${output_path}"/"${output_name}" | "${zstd_bin}" -T0 -o "${output_path_user}"/"${output_name}".tar.zst
                        #Testing tar with PIGZ
                        #time tar -cvf - "${output_path}"/"${output_name}" | "${pigz_bin}" > "${output_path_user}"/"${output_name}".tar.pigz
                        echo "zstd_tunnel_compression have finished successed"
                    fi
                else
                    echo "zstd doesn't exist or you don't have execute permission.."
                fi
            else
                #Cheking if the output name file is a directory.
                if [[ -n "${2}" ]]
                then
                    echo "Output name belongs to a directory.."
                    #Creating the output directory path name
                    output_path_user=$(dirname "${2}")
                    output_name=$(basename "${2}")
                    #Creating the folder structure
                    mkdir -p "${output_path_user}"
                    #Run zstd
                    zstd_bin=$(which zstd)
                    if [[ -x "${zstd_bin}" ]]
                    then
                        output_path_dir=$(dirname "${output_path}")
                        output_name_dir=$(basename "${output_path}")
                        cd "${output_path_dir}"/
                        echo "Running zstd.."
                        #Checking turbo mode ON or OFF
                        if [[ -n "${3}" && "${3}" == "--turbo=off" ]]
                        then
                            echo "Turbo-mode OFF !!"
                            # Use the following line if you want to compress and move an entire directory NOT in tubo mode
                            tar -cvf - "${output_name_dir}"/ | "${zstd_bin}" -o "${output_path_user}"/"${output_name}".d.tar.zst
                            echo "zstd_tunnel_compression have finished successed"
                            cd -
                        else
                            echo "Turbo-mode ON (by default) !!"
                            # Use the following line if you want to compress and move an entire directory in tubo mode
                            tar -cvf - "${output_name_dir}"/ | "${zstd_bin}" -T0 -o "${output_path_user}"/"${output_name}".d.tar.zst
                            echo "zstd_tunnel_compression have finished successed"
                            cd -
                        fi
                    else
                        echo "zstd doesn't exist or you don't have execute permission.."
                    fi
                else
                    echo "Your output name is empty!"
                fi
            fi
        fi
    #Checking the number or parameters
    elif [[ "${#}" -gt 3 ]]
    then
        echo "Error: Too many arguments."
        echo "Try with --help option"
    elif [[ "${#}" -eq 0 ]]
    then
        echo "Error: You need to put at least the directory output to use in /scratch and the your output name (without spaces)."
        echo "Try with --help option"
        # Use the following line if you want to move a single file
        #        tar --use-compress-program "${zstd_bin}" -cf "${output_path}"/"${output_name}".tar.zst "${output_path}"
        #        mv "${output_path}"/"${output_name}".tar.zst "${output_path_home}"/"${output_name}"
        # Use the following line if you want to move an entire directory in tubo mode
        #        tar -cvf - "${output_path}"/ | "${zstd_bin}" -T0 --ultra -22 -o "${output_path}"/"${output_name}".tar.zst
    #Printing help section
    elif [[ "${1}" == "--help" || "${1}" == "-help" || "${1}" == "-h" || "${1}" == "--h" ]]
    then
        echo "-------------------------------------------------------------------------------------------"
        echo "Purpose: Use zstd with /scratch and tar for your"
        echo "-------  scripts outputs."
        echo
        echo "Positional arguments:"
        echo "---------------------"
        echo
        echo "First argument  (\$1): Directory for your outputs in /scratch/              // string"
        echo "Second argument (\$2): Your output name (file or directory)                 // string"
        echo "Third argument  (\$3): Turbo-mode (\"on\" or \"off\", default: --turbo=on)      // string"
        echo
        echo "Optional:"
        echo "---------"
        echo
        echo "    --help"
        echo
        echo "Common use:"
        echo "-----------"
        echo "    zstd_tunnel_compression <FOLDER-NAME-IN-SCRATCH> <YOUR-OUTPUT-NAME> --turbo=<on or off>"
        echo "-------------------------------------------------------------------------------------------"
    else
        if [[ -z "${1}" ]]
        then
            echo "Your directory given to use in /scratch is empty.."
            echo "Try with --help option"
        elif [[ -f "${1}" && -s "${1}" ]]
        then
            echo "Your directory given to use in /scratch is a file and is not empty !"
            echo "Try with other directory name or delete the file."
        fi
    fi
}

updatePrompt() {
: '
Taken from: https://github.com/pyenv/pyenv-virtualenv/issues/135#issuecomment-951074873
'
    if [[ -n $VIRTUAL_ENV ]]; then
        export PS1=$PS1
    elif [[ -n "$(command -v pyenv)" && -n "$(pyenv version-name)" ]]; then
        PYENV_NAME=$(pyenv version-name)
        LIGHT_BLUE="\[\e[01;34m\]"
        RESET="\[\e[0m\]"
        if [[ -n "$(command -v uv)" && -n "$(uv python pin 2>/dev/null)" ]]; then
            UV_PYTHON_PIN=$(uv python pin)
            export PS1=$LIGHT_BLUE'(pyenv:${PYENV_NAME//./}) (uv:${UV_PYTHON_PIN//./}) '$RESET$BASE_PROMPT
        else
            export PS1=$LIGHT_BLUE'(pyenv:${PYENV_NAME//./}) '$RESET$BASE_PROMPT
        fi
    else
        export PS1=$BASE_PROMPT
    fi
}




