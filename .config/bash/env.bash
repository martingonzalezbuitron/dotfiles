export EDITOR=vim
export PAGER=less
#export LANG=en_US.UTF-8

# 2024-06-27 MGB
# My personal VARIABLES
export VIMINIT="source ~/.vim/.vimrc"
# Environment variable for LESSOPEN with source-highlight
export LESSOPEN="| /usr/local/bin/src-hilite-lesspipe.sh %s"
export LESS=" -R "
# Enviroment variable for MANPAGER
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# Enviroment variable for Postgresql-15.3
export LD_LIBRARY_PATH=/opt/postgresql-15.3/pgsql/lib
export MANPATH=/opt/postgresql-15.3/pgsql/share/man:$MANPATH
# Node.js due pylance in VSCODE crushes (https://github.com/microsoft/pylance-release/blob/main/TROUBLESHOOTING.md#pylance-is-crashing)
export NODE_OPTIONS="--max-old-space-size=8192"

# Set the default browser.
if [ `uname` == "Linux" ]; then
    export BROWSER=google-chrome # Ubuntu, use system browser
elif [ -x $(command -v chrome) ]; then
    export BROWSER=firefox # Chrome
fi

# Set pyenv version prompts
export PYENV_VIRTUALENV_DISABLE_PROMPT=1
export BASE_PROMPT=$PS1
export PROMPT_COMMAND='updatePrompt'

# Environment variable for ghcup
# Taken from: https://wiki.archlinux.org/title/XDG_Base_Directory
# After this, ghcup will use XDG_DATA_HOME and XDG_CONFIG_HOME
# Do not forget to set up the binary using this command:
# curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
# During binary execution set the following values:
#     Do you want to install haskell-language-server (HLS)? --> Yes
#     Do you want to enable better integration of stack with GHCup? --> Yes
# ===============================================================================
# OK! /home/martingb/.config/bash/env.bash has been modified. Restart your terminal for the changes to take effect,
# or type "source /home/martingb/.local/share/ghcup/env" to apply them in your current terminal session.
# ===============================================================================
# All done!
# To start a simple repl, run:
#   ghci
# To start a new haskell project in the current directory, run:
#   cabal init --interactive
# To install other GHC versions and tools, run:
#   ghcup tui
# If you are new to Haskell, check out https://www.haskell.org/ghcup/steps/
export GHCUP_USE_XDG_DIRS=true
[ -f "/home/martingb/.local/share/ghcup/env" ] && source "/home/martingb/.local/share/ghcup/env" # ghcup-env

# Set new libs for Boot c++
#export INCLUDE="/opt/boost_v1.78:$INCLUDE"
#export LIBRARY_PATH="/opt/boost_v1.78/stage/lib:$LIBRARY_PATH"
