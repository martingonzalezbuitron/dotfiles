# Managing MY DOTFILES

## Most taken from:
- [Hack News](https://news.ycombinator.com/item?id=11071754)
- [Atlassian tutorial](https://www.atlassian.com/git/tutorials/dotfiles)
- [DistroTube tutorial](https://www.youtube.com/watch?v=tBoLDpTWVOM)


This use `git bare` strategy but considering first check
how is `$HOME` (i.e. new `$HOME`?, not-new `$HOME`).

## Requirements:
* **curl**
* **git**

## Usage - Follow these steps in order

**1. Do you already have a git remote repo with your dotfiles? If the answer is NO, go to step 2.**  
**1.1. Are you already using git bare strategy in this PC? If the answer is NO, go to step 1.2.**

Use git bare repository with your alias "mydotfiles" without any problem, e.g.:
```
mydotfiles status
mydotfiles add .vimrc
mydotfiles commit -m "Add vimrc"
mydotfiles add .config/bash/functions.bash
mydotfiles commit -m "Add bash functions"
mydotfiles push
```

If you want to know about multiples branches, go to step 3. Otherwise, stop reading this file, anyway you are doing well.

**1.2. Do you have some dotfiles present (example, Linux's defaults) in this PC but are not from you or belongs to another PC's configuration? If the answer is NO, start all over again because you messed up something on the way.**

Ok, let's start doing this...

Check if you have installed `curl`, run:
```
if [[ -e $(which curl) ]]; then echo "Nice, you have curl installed.."; else echo "Install curl first, buddy !"; fi
```

The magic step, run:
```
curl -Lks https://bit.do/clonemydotfiles | bash
```

#### **Note:**

Git bare repository strategy will fail if your home directory isn't empty.  
To get around that, above running step does a backup as it is showed below.

BACKUPING your dotfiles (you never know what could happend, and is a good practice before delete something), like this:

```
mkdir -p ~/.dotfilesBackup-"$(date +%Y-%m-%d)" && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} ~/.dotfilesBackup-"$(date +%Y-%m-%d)"/{}
```

Now, your dotfiles were replicated. You should read 1.1 and have fun !  
If you want to know about multiples branches, go to step 3. Otherwise, stop reading this file, anyway you are doing well.

**2. Start creating the bare repository from ZERO !**

Creating the git bare repo:
```
mkdir -p $HOME/.dotfiles
git init --bare $HOME/.dotfiles
#echo ".dotfiles" >> $HOME/.gitignore  <--- # I don't know if this line is necessary because further we will use "--local" flag.
```

If you didn't do it yet, go to your .bashrc and put the following alias for your dotfiles:
```
#alias mydotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
```
Remember to load `bashrc` again or `profile`:"
```
source ~/.profile
#source ~/.bashrc
```
Or create a git alias instead of bash: (I think this is the clever way but need to use "git" first)
```
git config --global alias.mydotfiles "--git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
```

Git bare repo settings (just add what you want!)
```
mydotfiles config --local status.showUntrackedFiles no
```

Now, you have your dotfiles created but only locally!  
To create and make it remotely for example in GitLab, run:
```
mydotfiles push --set-upstream git@gitlab.com:martingonzalezbuitron/dotfiles.git master
```

Nice!, if you want to know about multiples branches, go to step 3. Otherwise, stop reading this file, anyway you are doing well. Have fun !

**3. Working with multiple branches:**

Git bare repository allows you to work with different branches.  
Suppose you have 4 PCs (hey, lucky you):

    PC1 - Cluster                   // remote ssh
    PC2 - Personal laptop           // local
    PC3 - Personal desktop          // local
    PC4 - Parent or friend          // local

You can create multiple branchs from `master` and personalice different configurations files in each branch.  
When working on a PC, remember to check which branch you are currently in before commit and then push. Same when you want to pull.

Example: I have a `master` branch and some "computer" branches. When changes are required for all computers, I do it in `master`, and then update each branch by doing a `git merge master`.


END of File
===========

Written by Martín González Buitrón  
Email: martingonzalezbuitron@gmail.com