# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=2000

# append to the history file, don't overwrite it
shopt -s histappend
# Taken from: https://unix.stackexchange.com/questions/18212/bash-history-ignoredups-and-erasedups-setting-conflict-with-common-history/18443#18443
PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# Make less more friendly for non-text input files, see lesspipe(1)
# Open bunch of files (*.a,*.arj,*.tar.bz2,*.bz,*.bz2,*.deb,*.udeb,
# *.ddeb,*.doc,*.gif,*.jpeg,*.jpg,*.pcd,*.png,*.tga,*.tiff,*.tif,
# *.iso,*.raw,*.bin,*.lha,*.lzh,*.tar.lz,*.tlz,*.lz,*.7zi,*.pdf,
# *.rar,*.r[0-9][0-9],*.rpm,*.tar.gz,*.tgz,*.tar.z,*.tar.dz,*.gz,
# *.z,*.dz,*.tar,*.tar.xz,*.xz,*.jar,*.war,*.xpi,*.zip,*.zoo)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm*|rxvt*)
        color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        echo "WARNING: force_color_prompt is set to yes but tput is not installed or does not support setaf"
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    # Main info: https://www.gnu.org/software/bash/manual/bash.html#Controlling-the-Prompt
    # Non-printing escape sequences have to be enclosed in \[\033[ and \].
    # For colour escape sequences, they should also be followed by a lowercase m.
    # 033 => ESC character in Octal (see: https://www.asciitable.com/)
    # \e => escape character in Hexadecimal (see: https://www.asciitable.com/)
    # https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
    # http://jafrog.com/2013/11/23/colors-in-terminal.html
    # http://lucentbeing.com/writing/archives/a-guide-to-256-color-codes/
    WHITE="\[\e[01;37m\]"
    LIGHT_GREEN="\[\e[01;32m\]"
    LIGHT_BLUE="\[\e[01;34m\]"
    RESET="\[\e[0m\]"
    # [38;5;⟨n⟩m Select foreground color      where n is a number from 256-colortable
    # [48;5;⟨n⟩m Select background color
    LIGHT_PINK="\[\e[01;38;05;201m\]"
    ORANGE="\[\e[01;38;05;202m\]"
    RED="\[\e[01;38;05;196m\]"
    PURPLE="\[\e[01;38;05;129m\]"
    GREY="\[\e[01;38;05;250m\]"
    BLUE="\[\e[01;38;05;27m\]"
    GOLD="\[\e[01;38;05;214m\]"
    oldIFS="$IFS"; IFS="-"
    h_color=""
    read -ra hArray <<< "${HOSTNAME}"
    for i in "${hArray[@]}"; do
        case "$i" in
            (D)
                computer="${PURPLE}D";;
            (K)
                computer="${RED}K";;
            (MGB)
                owner="${LIGHT_GREEN}-MGB";;
            (SBG)
                owner="${LIGHT_GREEN}-${GREY}S${RED}B${BLUE}G";;
            (U2204)
                distr="${LIGHT_GREEN}-U2204";;
            (*) # Default, WARNING + do nothing
                h_color="[${WHITE}UNKNOWN HOST${LIGHT_GREEN}] => ${LIGHT_PINK}'${HOSTNAME}'"
                break
                ;;
        esac
    done; IFS="$oldIFS"
    if [[ -z "${h_color}" ]]; then
        h_color="${computer}${owner}${distr}"
    fi
    PS1="${debian_chroot:+($debian_chroot)}${LIGHT_GREEN}\u@${h_color}${RESET}:${LIGHT_BLUE} \w/${ORANGE}\$(__git_ps1)${RESET} \$ "
else
    PS1="${debian_chroot:+($debian_chroot)}\u@\h:\w\$(__git_ps1)\$ "
fi
unset color_prompt force_color_prompt

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -d $XDG_CONFIG_HOME/bash ]; then
    # Set up all personal settings - MGB
    source $XDG_CONFIG_HOME/bash/aliases.bash
    source $XDG_CONFIG_HOME/bash/bash-completions.bash
    source $XDG_CONFIG_HOME/bash/env.bash
    source $XDG_CONFIG_HOME/bash/colors.bash
    source $XDG_CONFIG_HOME/bash/funtions.bash
    source $XDG_CONFIG_HOME/bash/prompt.bash
fi

# Run eval for softwares
# pyenv and pyenv-virtualenv
if command -v pyenv 1>/dev/null 2>&1
then
    eval "$(pyenv init --path)"
    eval "$(pyenv init -)"
    #eval "$(pyenv virtualenv-init -)"
fi
# pipx
if command -v pipx 1>/dev/null 2>&1
then
    eval "$(register-python-argcomplete pipx)"
fi


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

# Deactive bla bla to enable key-binding for forward search on bash history
# This automatically frees up ctrl+s for forward-search-history
# Taken from: https://stackoverflow.com/questions/791765/unable-to-forward-search-bash-history-similarly-as-with-ctrl-r
stty stop '^P'
